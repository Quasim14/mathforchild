# MATHFORCHILD CHANGE LOG by Ibbtek

## Version 2.3

- Include & read Help, About & Licenses file from inside the *.jar
- Minor code change

## Version 2.2

- Change the About and Help Windows
- Solve Linux problem compatibility
